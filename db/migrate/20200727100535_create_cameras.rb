class CreateCameras < ActiveRecord::Migration[5.2]
  def change
    create_table :cameras do |t|
      t.string :camera_type
      t.string :company_type
      t.string :use_history
      t.string :condition
      t.string :listing_name
      t.text :summary
      t.string :address
      t.boolean :is_camera_case
      t.boolean :is_camera_leg
      t.boolean :is_light
      t.integer :price
      t.boolean :active
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
