class ReservationsController < ApplicationController
  before_action :authenticate_user!

  def create
    camera = Camera.find(params[:camera_id])

    if current_user == camera.user
      flash[:alert] = "Share予約できていません!"
    else
      start_date = Date.parse(reservation_params[:start_date])
      end_date = Date.parse(reservation_params[:end_date])
      days = (end_date - start_date).to_i + 1

      @reservation = current_user.reservations.build(reservation_params)
      @reservation.camera = camera
      @reservation.price = camera.price
      @reservation.total = camera.price * days
      @reservation.save

      flash[:notice] = "Share予約できました!"
    end
    redirect_to camera
  end

  def your_trips
    @trips = current_user.reservations.order(start_date: :asc)
  end

  def your_reservations
    @cameras = current_user.cameras
  end

  private
    def reservation_params
      params.require(:reservation).permit(:start_date, :end_date)
    end
end
