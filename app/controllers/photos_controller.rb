class PhotosController < ApplicationController

  def create
    @camera = Camera.find(params[:camera_id])

    if params[:images]
      params[:images].each do |img|
        @camera.photos.create(image: img)
      end

      @photos = @camera.photos
      redirect_back(fallback_location: request.referer, notice: "アップロードできました！")
    end
  end

  def destroy
    @photo = Photo.find(params[:id])
    @camera = @photo.camera

    @photo.destroy
    @photos = Photo.where(camera_id: @camera.id)

    respond_to :js
  end
end
