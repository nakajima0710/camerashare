class CamerasController < ApplicationController
  before_action :set_camera, except: [:index, :new, :create]
  before_action :authenticate_user!, except: [:show]
  before_action :is_authorised, only: [:listing, :pricing, :description, :photo_upload, :amenities, :location, :update]

  def index
    @cameras = current_user.cameras
  end

  def new
    @camera = current_user.cameras.build
  end

  def create
    @camera = current_user.cameras.build(camera_params)
    if @camera.save
      redirect_to listing_camera_path(@camera), notice: "登録しました！"
    else
      flash[:alert] = "登録できていません！"
      render :new
    end
  end

  def show
    @photos = @camera.photos
  end

  def listing
  end

  def pricing
  end

  def description
  end

  def photo_upload
    @photos = @camera.photos
  end

  def amenities
  end

  def location
  end

  def update

    new_params = camera_params
    new_params = camera_params.merge(active: true) if is_ready_camera

    if @camera.update(camera_params)
      flash[:notice] = "保存しました！"
    else
      flash[:alert] = "保存できていません！"
    end
    redirect_back(fallback_location: request.referer)
  end


  # --- Reservations ---
  def preload
    today = Date.today
    reservations = @camera.reservations.where("start_date >= ? OR end_date >= ?", today, today)

    render json: reservations
  end

  def preview
    start_date = Date.parse(params[:start_date])
    end_date = Date.parse(params[:end_date])

    output = {
      conflict: is_conflict(start_date, end_date, @camera)
    }

    render json: output
  end

  def your_trips
    @trips = current_user.reservations.order(start_date: :asc)
  end

  def your_reservations
    @cameras = current_user.cameras
  end


  private

    def is_conflict(start_date, end_date, camera)
      check = camera.reservations.where("? < start_date AND end_date < ?", start_date, end_date)
      check.size > 0? true : false
    end

    def set_camera
      @camera = Camera.find(params[:id])
    end

    def is_authorised
      redirect_to root_path, alert: "You don't have permission" unless current_user.id == @camera.user_id
    end

    def is_ready_camera
      !@camera.active && !@camera.price.blank? && !@camera.listing_name.blank? && !@camera.photos.blank? && !@camera.address.blank?
    end

    def camera_params
      params.require(:camera).permit(:camera_type, :company_type, :use_history, :condition, :listing_name, :summary, :address, :is_camera_case, :is_camera_leg, :is_light, :price, :active, :latitude, :longitude)
    end
end
