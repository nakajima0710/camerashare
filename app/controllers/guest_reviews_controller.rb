class GuestReviewsController < ApplicationController

  def create
    # Step 1: Check if the reservation exist (camera_id, host_id, host_id)

    # Step 2: Check if the current host already reviewed the guest in this reservation.

    @reservation = Reservation.where(
                    id: guest_review_params[:reservation_id],
                    camera_id: guest_review_params[:camera_id]
                   ).first

    if !@reservation.nil? && @reservation.camera.user.id == guest_review_params[:host_id].to_i

      @has_reviewed = GuestReview.where(
                        reservation_id: @reservation.id,
                        host_id: guest_review_params[:host_id]
                      ).first

      if @has_reviewed.nil?
          # Allow to review
      #    @guest_review = GuestReview.create(guest_review_params)
          @guest_review = current_user.guest_reviews.new(guest_review_params)
          flash[:success] = "レビューできました！"
      else
          # Already reviewed
          flash[:success] = "すでにレビューしています！"
      end
    else
      flash[:alert] = "レビューできていません！"
    end

    redirect_back(fallback_location: request.referer)
  end

  def destroy
    @guest_review = Review.find(params[:id])
    @guest_review.destroy

    redirect_back(fallback_location: request.referer, notice: "Removed...!")
  end

  private
    def guest_review_params
      params.require(:guest_review).permit(:comment, :star, :camera_id, :reservation_id, :host_id).merge(guest_id: current_user.id)
  #    params.require(:guest_review).permit(:comment, :star, :camera_id, :reservation_id, :host_id)
    end
end
