class UsersController < ApplicationController
  def show
    @user = User.find(params[:id])
    @cameras = @user.cameras
  end
end
